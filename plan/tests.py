#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


from todo_tracker.exceptions.exceptions import UserError, TaskError, ConfigError
from todo_tracker.reminder_manager.reminder_manager import ReminderManager
from todo_tracker.task_manager.task_manager import TaskManager
from todo_tracker.user_manager.user_manager import UserManager
from todo_tracker.exceptions.exceptions import TaskError
from todo_tracker.reminder.reminder import Reminder
from todo_tracker.storage.storage import Storage
from todo_tracker.tasks.task import Task
from datetime import datetime, timedelta
from todo_tracker.user.user import User
import unittest
import tempfile
import shutil
import json
import os


class UnitTests(unittest.TestCase):
    def setUp(self):
        self.__tracker_dir = tempfile.mkdtemp()
        self.info_storage = Storage(self.__tracker_dir + '/', self.__tracker_dir + '/', self.__tracker_dir + '/')
        self.task_manager = TaskManager(self.info_storage)
        self.user_manager = UserManager(self.info_storage)
        self.reminder_manager = ReminderManager(self.info_storage)
        with open(os.path.join(self.info_storage.r_path, 'reminder.json'), 'w') as j_file:
            json.dump([], j_file)
        with open(os.path.join(self.info_storage.t_path, 'sub_tasks.json'), 'w') as j_file:
            json.dump([], j_file)
        with open(os.path.join(self.info_storage.t_path, 'tasks.json'), 'w') as j_file:
            json.dump([], j_file)
        with open(os.path.join(self.info_storage.us_path, 'users.json'), 'w') as j_file:
            json.dump({}, j_file)

    def tearDown(self):
        shutil.rmtree(self.__tracker_dir)

    def test_create_user(self):
        user = User('darya', 'password', 'Darya')
        self.user_manager.create_new_user(user.login, user.password, user.name)
        content = {'darya': {
            'password': 'password',
            'user_personality': 'Darya'
        }}
        users_dict = self.info_storage.load_user_by_id('darya')
        self.assertEqual(users_dict, content)

    def test_authorization(self):
        user = User('darya', 'password', 'Darya')
        self.user_manager.create_new_user(user.login, user.password, user.name)
        self.assertTrue(self.user_manager.authorization(user.login, user.password))

    def test_create_head_task(self):
        task = Task('Task1', 1, 'Work', None, 'darya')
        content = {
                    'content': 'Task1',
                    'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M'),
                    'execution_date': (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
                    'group': 'Work',
                    'id': 1,
                    'priority': 1,
                    'status': 'Created',
                    'owner': 'darya'
                    }
        self.task_manager.create_new_task('darya', False, None, task.content,
                                          task.priority, task.group, task.execution_date)
        tasks_dict = self.info_storage.load_task_by_id('darya', False, 1)
        self.assertEqual(tasks_dict, content)

    def test_edit_head_task(self):
        task = Task('Task1', 1, 'Work', None, 'darya')
        content = {
                    'content': 'Task1',
                    'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M'),
                    'execution_date': (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
                    'group': 'Work',
                    'id': 1,
                    'priority': 4,
                    'status': 'Done',
                    'owner': 'darya'
                    }
        self.task_manager.create_new_task('darya', False, None, task.content, task.priority,
                                          task.group, task.execution_date)
        self.task_manager.edit_task('darya', False, 1, None, 4, None, None, 'Done')
        tasks_dict = self.info_storage.load_task_by_id('darya', False, 1)
        self.assertEqual(tasks_dict, content)

    def test_remove_head_task(self):
        task = Task('Task1', 1, 'Work', None, 'darya')
        self.task_manager.create_new_task('darya', False, None, task.content, task.priority,
                                          task.group, task.execution_date)
        self.task_manager.remove_task('darya', False, 1)
        tasks_dict = self.info_storage.load_task_by_id('darya', False, 1)
        self.assertFalse(tasks_dict)

    def test_create_sub_task(self):
        task = Task('sub1', 3, 'Personal', None, 'darya')
        content = {
                    'content': 'sub1',
                    'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M'),
                    'execution_date': (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
                    'group': 'Personal',
                    'id': 1,
                    'priority': 3,
                    'status': 'Created',
                    'owner': 'darya',
                    'parent_id': 1
            }
        self.task_manager.create_new_task('darya', True, 1, task.content, task.priority,
                                          task.group, task.execution_date)
        tasks_dict = self.info_storage.load_task_by_id('darya', True, 1)
        self.assertEqual(tasks_dict, content)

    def test_edit_sub_task(self):
        task = Task('sub', 1, 'Work', None, 'darya')
        content = {
                'content': 'SUB',
                'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M'),
                'execution_date': (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
                'group': 'Personal',
                'owner': 'darya',
                'parent_id': 1,
                'id': 1,
                'priority': 2,
                'status': 'Created'
                }
        self.task_manager.create_new_task('darya', True, 1, task.content, task.priority,
                                          task.group, task.execution_date)
        self.task_manager.edit_task('darya', True, 1, 'SUB', 2, 'Personal', None, None)
        tasks_dict = self.info_storage.load_task_by_id('darya', True, 1)
        self.assertEqual(tasks_dict, content)

    def test_remove_sub_task(self):
        task = Task('Something', 1, 'Work', None, 'darya')
        content = [
            {
                "content": "sub1",
                "date_of_create": datetime.now().strftime('%d.%m.%Y %H:%M'),
                "execution_date": (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
                "group": "Personal",
                'owner': 'darya',
                'parent_id': 2,
                "id": 1,
                "priority": 1,
                "status": "Created"
            },
            {
                "content": "sub2",
                "date_of_create": datetime.now().strftime('%d.%m.%Y %H:%M'),
                "execution_date": (datetime.now() + timedelta(hours=12)).strftime('%d.%m.%Y %H:%M'),
                "group": "Personal",
                'owner': 'darya',
                'parent_id': 2,
                "id": 2,
                "priority": 1,
                "status": "Done"
            },
            {
                "content": "sub2",
                "date_of_create": datetime.now().strftime('%d.%m.%Y %H:%M'),
                "execution_date": (datetime.now() + timedelta(days=41)).strftime('%d.%m.%Y %H:%M'),
                "group": "Personal",
                'owner': 'darya',
                'parent_id': 2,
                "id": 3,
                "priority": 2,
                "status": "Created"
            }
            ]
        self.task_manager.create_new_task('darya', True, 2, 'sub1', 1, 'Personal', task.execution_date)
        self.task_manager.create_new_task('darya', True, 2, 'sub2', 1, 'Personal',
                                          (datetime.now() + timedelta(hours=12)).strftime('%d.%m.%Y %H:%M'))
        self.task_manager.create_new_task('darya', True, 2, 'sub2', 2, 'Personal',
                                          (datetime.now() + timedelta(days=41)).strftime('%d.%m.%Y %H:%M'))
        self.task_manager.create_new_task('darya', True, 2, task.content, task.priority, task.group,
                                          task.execution_date)
        self.task_manager.edit_task('darya', True, 2, None, None, None, None, 'Done')
        self.task_manager.remove_task('darya', True, 4)
        tasks_dict = self.info_storage.load_user_tasks(True, 'darya')
        self.assertEqual(tasks_dict, content)

    def test_load_tasks_without_rights(self):
        task = Task('Task1', 1, 'Work', None, 'darya')
        self.task_manager.create_new_task('darya', False, None, task.content, task.priority,
                                          task.group, task.execution_date)
        with self.assertRaises(UserError):
            self.info_storage.load_task_by_id('NotDarya', False, 1)

    def test_edit_nonexistent_task(self):
        with self.assertRaises(TaskError):
            self.task_manager.edit_task('darya', False, 2, 'something', 1, None, None, 'Done')

    def test_remove_nonexistent_task(self):
        with self.assertRaises(TaskError):
            self.task_manager.remove_task('darya', False, 1)

    def test_create_existing_user(self):
        user = User('darya', 'password', 'Darya')
        self.user_manager.create_new_user(user.login, user.password, user.name)
        with self.assertRaises(UserError):
            self.user_manager.create_new_user(user.login, user.password, user.name)

    def test_authorization_fail(self):
        user = User('darya', 'password', 'Darya')
        self.user_manager.create_new_user(user.login, user.password, user.name)
        user.login = 'darya1'
        with self.assertRaises(UserError):
            self.user_manager.authorization(user.login, user.password)

    def test_incorrect_date_of_create(self):
        with self.assertRaises(TaskError):
            Task('content', 1, 'Work', [(datetime.today() - timedelta(days=1)).strftime('%d.%m.%Y'),
                                        datetime.now().strftime('%H:%M')], 'darya')

    def test_remove_head_task_with_subs(self):
        task = Task('TAsk1', None, None, None, 'darya')
        self.task_manager.create_new_task('darya', False, None, task.content, task.priority,
                                          task.group, task.execution_date)
        task.content = 'Sub1'
        self.task_manager.create_new_task('darya', True, 1, task.content, task.priority,
                                          task.group, task.execution_date)
        self.task_manager.remove_task('darya', False, 1)
        task = self.info_storage.load_task_by_id('darya', False, 1)
        sub = self.info_storage.load_task_by_id('darya', True, 1)
        self.assertFalse(task)
        self.assertFalse(sub)

    def test_edit_head_task_status_to_Done(self):
        task = Task('Task1', 1, 'Work', None, 'darya')
        task_content = {
            'content': 'Task1',
            'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M'),
            'execution_date': (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
            'group': 'Work',
            'id': 1,
            'priority': 4,
            'status': 'Done',
            'owner': 'darya'
        }
        self.task_manager.create_new_task('darya', False, None, task.content, task.priority,
                                          task.group, task.execution_date)
        sub = Task('sub1', 3, 'Personal', None, 'darya')
        sub_content = {
            'content': 'sub1',
            'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M'),
            'execution_date': (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
            'group': 'Personal',
            'id': 1,
            'priority': 3,
            'status': 'Done',
            'owner': 'darya',
            'parent_id': 1
        }
        self.task_manager.create_new_task('darya', True, 1, sub.content, sub.priority,
                                          sub.group, sub.execution_date)
        self.task_manager.edit_task('darya', False, 1, None, 4, None, None, 'Done')
        tasks_dict = self.info_storage.load_task_by_id('darya', False, 1)
        sub_dict = self.info_storage.load_task_by_id('darya', True, 1)
        self.assertEqual(tasks_dict, task_content)
        self.assertEqual(sub_dict, sub_content)

    def test_edit_head_task_status_to_Failed(self):
        task = Task('Task1', 1, 'Work', None, 'darya')
        task_content = {
            'content': 'Task1',
            'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M'),
            'execution_date': (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
            'group': 'Work',
            'id': 1,
            'priority': 4,
            'status': 'Failed',
            'owner': 'darya'
        }
        self.task_manager.create_new_task('darya', False, None, task.content, task.priority,
                                          task.group, task.execution_date)
        sub = Task('sub1', 3, 'Personal', None, 'darya')
        sub_content = {
            'content': 'sub1',
            'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M'),
            'execution_date': (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
            'group': 'Personal',
            'id': 1,
            'priority': 3,
            'status': 'Failed',
            'owner': 'darya',
            'parent_id': 1
        }
        self.task_manager.create_new_task('darya', True, 1, sub.content, sub.priority,
                                          sub.group, sub.execution_date)
        self.task_manager.edit_task('darya', False, 1, None, 4, None, None, 'Failed')
        tasks_dict = self.info_storage.load_task_by_id('darya', False, 1)
        sub_dict = self.info_storage.load_task_by_id('darya', True, 1)
        self.assertEqual(tasks_dict, task_content)
        self.assertEqual(sub_dict, sub_content)

    def test_add_reminder(self):
        task = Task('Task1', 1, 'Work', None, 'darya')
        self.task_manager.create_new_task('darya', False, None, task.content,
                                          task.priority, task.group, task.execution_date)
        reminder = Reminder('WR', 'darya', None)
        self.reminder_manager.create_new_reminder(None, reminder.owner, reminder.week_day, False, reminder.frequency)
        new_reminder = {'task_id': 1,
                        'owner': 'darya',
                        'parent_id': None,
                        'frequency': 'WR',
                        'id': 1,
                        'week_day': None,
                        'last_update': datetime.today().strftime('%d.%m.%Y')
                        }
        reminder = self.info_storage.load_task_reminder(1, None, 'darya')
        self.assertEqual(reminder, new_reminder)

    def test_remove_reminder(self):
        task = Task('Task1', 1, 'Work', None, 'darya')
        self.task_manager.create_new_task('darya', False, None, task.content,
                                          task.priority, task.group, task.execution_date)
        reminder = Reminder(None, 'darya', None)
        self.reminder_manager.create_new_reminder(None, reminder.owner, reminder.week_day, False, reminder.frequency)
        self.reminder_manager.remove_reminder(False, None, 1, 'darya')
        reminder = self.info_storage.load_task_reminder(1, None, 'darya')
        self.assertFalse(reminder)

    def test_add_reminder_for_sub_task(self):
        task = Task('Task1', 1, 'Work', None, 'darya')
        self.task_manager.create_new_task('darya', False, None, task.content,
                                          task.priority, task.group, task.execution_date)
        reminder = Reminder('WR', 'darya', None)
        self.reminder_manager.create_new_reminder(None, reminder.owner, reminder.week_day, False, reminder.frequency)
        new_reminder = {'task_id': 1,
                        'owner': 'darya',
                        'id': 1,
                        'parent_id': None,
                        'frequency': 'WR',
                        'week_day': None,
                        'last_update': datetime.today().strftime('%d.%m.%Y')
                        }
        sub = Task('Sub1', 1, 'Work', None, 'darya')
        self.task_manager.create_new_task('darya', True, 1, sub.content, sub.priority, sub.group, sub.execution_date)
        sub_reminder = Reminder('ED', 'darya', None)
        self.reminder_manager.create_new_reminder(1, sub_reminder.owner, sub_reminder.week_day,
                                                  True, sub_reminder.frequency)
        sub_reminder = {'task_id': 1,
                        'owner': 'darya',
                        'id': 2,
                        'parent_id': 1,
                        'frequency': 'ED',
                        'week_day': sub_reminder.week_day,
                        'last_update': datetime.today().strftime('%d.%m.%Y')
                        }
        reminder = self.info_storage.load_task_reminder(1, None, 'darya')
        new_sub_rem = self.info_storage.load_task_reminder(1, 1, 'darya')
        self.assertEqual(reminder, new_reminder)
        self.assertEqual(new_sub_rem, sub_reminder)