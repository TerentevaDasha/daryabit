#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

import argparse


class TrackParse:
    def __init__(self):
        self.login = None
        self.password = None
        self.name = None
        self.content = None
        self.priority = None
        self.group = None
        self.status = None
        self.sid = None
        self.hid = None
        self.execution_date = None
        self.user_flag = None
        self.task_action = None
        self.sub_flag = None
        self.frequency = None
        self.week_day = None
        self.query = None

        self.__parse_args()

    def __parse_args(self):
        parser = argparse.ArgumentParser(description='Simple todo tracker')

        user_group = parser.add_argument_group()
        user_group.add_argument('-cu', '--create_user', action='store_true', dest='user_flag',
                                default=False, help='Create new user flag')
        user_group.add_argument('-ul', '--user_login', dest='user_login', help='Set user login')
        user_group.add_argument('-up', '--user_password', dest='user_password', help='Set user password')
        user_group.add_argument('-un', '--user_name', dest='user_name', help='Set user name')

        task_group = parser.add_argument_group()
        task_group.add_argument('-ta', '--task_action', choices=['create', 'edit', 'show', 'remove'], default=None,
                                dest='task_action', help='Set action for task')
        task_group.add_argument('-q', '--query', dest='query', choices=['ALL', 'ID', 'TREE'], default='ALL',
                                help='Set query for task view')
        task_group.add_argument('-c', '--content', dest='content', help='Set task goal')
        task_group.add_argument('-p', '--priority', dest='priority', type=int, choices=range(1, 5), default=None,
                                help='Set task priority')
        task_group.add_argument('-s', '--status', dest='status', choices=['Done', 'Created', 'Failed'], default=None,
                                help='Set status for task')
        task_group.add_argument('-g', '--group', dest='group', choices=['Work', 'Personal'],
                                default=None, help='Set task group')
        task_group.add_argument('-ed', '--execution_date', dest='execution_date', nargs='*',
                                default=None, help='Set date of execution')
        task_group.add_argument('-hid', '--head_id', dest='hid', type=int, help='Set the head task id')
        task_group.add_argument('-sid', '--sub_id', dest='sid', type=int,  help='Set sub task id')
        task_group.add_argument('-st', '--sub_task', action='store_true', default=False, dest='sub_flag',
                                help='Set sub task flag')
        task_group.add_argument('-f', '--frequency', choices=['WR', 'ED', 'EW'], default='WR', dest='frequency',
                                help='Sets the frequency of the task')
        task_group.add_argument('-wd', '--week_day', choices=['Mon', 'Tue', 'Wen', 'Thu', 'Fri', 'Sat', 'Sun'],
                                default=None, dest='week_day', help='Set day of the week')

        args = parser.parse_args()
        self.login = args.user_login
        self.password = args.user_password
        self.name = args.user_name
        self.content = args.content
        self.priority = args.priority
        self.group = args.group
        self.status = args.status
        self.execution_date = args.execution_date
        self.sid = args.sid
        self.hid = args.hid
        self.user_flag = args.user_flag
        self.task_action = args.task_action
        self.sub_flag = args.sub_flag
        self.frequency = args.frequency
        self.week_day = args.week_day
        self.query = args.query