#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


from todo_tracker.checkers.task_prop_check import is_active_status, is_execution_today, status_check
from todo_tracker.exceptions import exceptions
from datetime import datetime, timedelta
import calendar
import logging


class ReminderManager:
    def __init__(self, storage):
        self.storage = storage

    def create_new_reminder(self, parent_id, owner, week_day, sub_flag, frequency):
        tasks = self.storage.load_user_tasks(sub_flag, owner)
        task_id = tasks[-1].get('id')
        new_reminder = {'task_id': task_id,
                        'owner': owner,
                        'parent_id': parent_id,
                        'frequency': frequency,
                        'week_day': week_day,
                        'last_update': datetime.today().strftime('%d.%m.%Y')
                        }
        self.storage.add_new_remind(new_reminder)

    def remove_reminder(self, sub_flag, hid, sid,  owner):
        rem = self.storage.load_task_reminder(sid, hid, owner)
        if rem:
            self.storage.pop_reminder(rem)
            logging.info('The reminder was successfully deleted')
        else:
            raise exceptions.TaskError('No task with such id')
        if not sub_flag:
            all_reminds = self.storage.load_user_reminds(owner)
            for reminder in all_reminds:
                if reminder.get('parent_id') == sid:
                    self.storage.pop_reminder(reminder)

    def get_notification_for_today(self, user_id):
        status_check(user_id, self.storage)
        task_list = list()
        sub_list = list()
        all_reminders = self.storage.load_user_reminds(user_id)
        date_today = datetime.today()
        for reminder in all_reminders:
            if not is_active_status(reminder.get('parent_id'), self.storage, reminder.get('task_id'), user_id):
                continue
            elif is_execution_today(reminder.get('parent_id'), self.storage, reminder.get('task_id'), user_id):
                if reminder.get('parent_id'):
                    sub_list.append(reminder.get('task_id'))
                else:
                    task_list.append(reminder.get('task_id'))
            else:
                if reminder.get('frequency') == 'WR':
                    continue
                elif reminder.get('frequency') == 'ED':
                    if reminder.get('parent_id'):
                        sub_list.append(reminder.get('task_id'))
                    else:
                        task_list.append(reminder.get('task_id'))
                else:
                    if calendar.day_abbr[date_today.weekday()] in reminder.get('week_day'):
                        if reminder.get('parent_id'):
                            sub_list.append(reminder.get('task_id'))
                        else:
                            task_list.append(reminder.get('task_id'))
        notifications = {'You have {0} tasks for today': len(task_list) + len(sub_list),
                         'of these, the head tasks have id: {0}': task_list,
                         'of these, the sub tasks have id: {0}': sub_list}
        return notifications

    def update_statuses(self, user_id):
        all_reminds = self.storage.load_user_reminds(user_id)
        for remind in all_reminds:
            task = self.storage.load_task_by_id(user_id, remind.get('parent_id'), remind.get('task_id'))
            if remind.get('last_update') != datetime.today().strftime('%d.%m.%Y'):
                if remind.get('frequency') == 'WR':
                    continue
                elif remind.get('frequency') == 'ED':
                    task.update({'status': 'In progress'})
                    remind.update({'last_update': datetime.today().strftime('%d.%m.%Y')})
                elif calendar.day_abbr[(datetime.today() - timedelta(days=1)).weekday()] in remind.get('week_day'):
                    task.update({'status': 'In progress'})
                    remind.update({'last_update': datetime.today().strftime('%d.%m.%Y')})
                self.storage.update_existed_reminder(remind)
                self.storage.update_existed_task(task.get('parent_id'), task)