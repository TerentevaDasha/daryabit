#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


from todo_tracker.task_manager.tasks_operations import show
from todo_tracker.checkers.task_prop_check import date_check, status_check
from todo_tracker.exceptions import exceptions
from datetime import datetime
import logging


class TaskManager:
    def __init__(self, storage):
        self.storage = storage

    def create_new_task(self, login, sub_flag, hid, content, priority, group, execution_date):
        """Creates a task for a specific user.  """
        execution_date = date_check(execution_date)
        new_task = {'content': content,
                    'owner': login,
                    'priority': priority,
                    'group': group,
                    'execution_date': execution_date.strftime('%d.%m.%Y %H:%M'),
                    'status': 'Created',
                    'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M')
                    }
        if sub_flag:
            new_task.update({'parent_id': hid})
        self.storage.add_new_task(sub_flag, new_task)
        logging.info('Task creation was successful')

    def remove_task(self, login, sub_flag, sid):
        """Deleting a task from a user by its id.  """
        task = self.storage.load_task_by_id(login, sub_flag, sid)
        if task:
            self.storage.pop_task(sub_flag, task)
            logging.info('The task was successfully deleted')
        else:
            raise exceptions.TaskError('No task with such id')
        if not sub_flag:
            """Deleting all sub tasks that the head task had.  """
            all_subs = self.storage.load_user_tasks(True, login)
            for i, task in enumerate(all_subs):
                if task.get('parent_id') == sid:
                    self.storage.pop_task(True, task)
            logging.info('All sub tasks are successfully deleted')

    def edit_task(self, login, sub_flag, hid, content, priority, group, execution_date, status):
        if execution_date:
            execution_date = date_check(' '.join(execution_date))
        task = self.storage.load_task_by_id(login, sub_flag, hid)
        if task:
            if content:
                task.update({'content': content})
            if priority:
                task.update({'priority': priority})
            if group:
                task.update({'group': group})
            if execution_date:
                task.update({'execution_date': execution_date.strftime('%d.%m.%Y %H:%M')})
            if status:
                task.update({'status': status})
            self.storage.update_existed_task(sub_flag, task)
            status_check(login, self.storage)
            logging.info('Task change was successful')
        else:
            raise exceptions.TaskError('No task with such id')

    def get_tasks_by_query(self, login, query, sub_flag, sid):
        if query == 'ALL':
            out_list = self.storage.load_user_tasks(sub_flag, login)
            return out_list
        elif query == 'TREE':
            out_elements = list()
            head_task = self.storage.load_task_by_id(login, sub_flag, sid)
            out_elements.append(head_task)
            if not sub_flag:
                subs = self.storage.load_user_tasks(True, login)
                out_elements.extend(subs)
            return out_elements
        elif query == 'ID':
            tasks = list()
            task = self.storage.load_task_by_id(login, sub_flag, sid)
            if task:
                tasks.append(task)
            return tasks