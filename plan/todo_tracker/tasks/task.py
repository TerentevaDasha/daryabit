#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


from todo_tracker.checkers.task_prop_check import date_check
from todo_tracker.exceptions import exceptions
from datetime import datetime, timedelta
import logging


class Task:

    def __init__(self, content, priority, group, execution_date, owner):
        self.task_owner = owner
        self.content = content
        self.priority = None
        self.group = None
        self.execution_date = None

        self.priority_validation(priority)
        self.group_validation(group)
        self.execution_date_validation(execution_date)

    def priority_validation(self, value):
        if value in range(1, 5):
            self.priority = value
        else:
            self.priority = 4

    def group_validation(self, value):
        if value in ['Work', 'Personal']:
            self.group = value
        else:
            self.group = 'Personal'

    def execution_date_validation(self, value):
        if value:
            if date_check(' '.join(value)):
                self.execution_date = ' '.join(value)
            else:
                raise exceptions.TaskError('Wrong date input!')
        else:
            self.execution_date = (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M')