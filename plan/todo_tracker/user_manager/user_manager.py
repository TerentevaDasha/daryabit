#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

from todo_tracker.exceptions import exceptions
import logging


class UserManager:
    def __init__(self, storage):
        self.storage = storage

    def create_new_user(self, login, password, name):
        user_info = self.storage.load_user_by_id(login)
        if not user_info:
            content = {login: {
                'user_personality': name,
                'password': password,
            }}
            self.storage.save_user(content)
            logging.info('User creation was successful')
        else:
            raise exceptions.UserError('The user with such login has already existed')

    def authorization(self, login, password):
        user_info = self.storage.load_user_by_id(login)
        if user_info:
            if login in user_info.keys() and password == user_info[login]['password']:
                return True
            else:
                raise exceptions.UserError('Wrong combination of username and password!')
        else:
            raise exceptions.UserError('No such user!')