#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-


from setuptools import setup, find_packages
from os.path import join, dirname


setup(
    name="todo_tracker",
    version="1.0",
    packages=find_packages(),
    long_description=open(join(dirname(__file__), "README.md")).read(),
    test_suite='tests',
    entry_points={
        'console_scripts':
            ['track = todo_tracker.core:main']
    }
)